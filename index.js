function cube (num) {
	const getCube = num**3;
	console.log(`The cube of ${num} is ${getCube}`);
}
cube(2);

let address = ['258', 'Washington Ave NW', 'California','90011'];
console.log(`I live at ${address[0]}, ${address[1]}, ${address[2]} ${address[3]}`);


let animal = {
		name: 'Lolong',
		species: 'Saltwater Crocodile',
		weight: '1075 kgs',
		measurement: '20 ft 3 in'
	};
	console.log(`${animal.name} was a ${animal.species}. He weighed at ${animal.weight} with a measurement of ${animal.measurement}.`);


const numbers = [1,2,3,4,5];
numbers.forEach((number) => {
	console.log(`${number} \n`)
});

// Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

let reduceNumber = numbers.reduce(function(a, b){
  return a + b;
});
console.log(reduceNumber);



class Dog {
	constructor(name, age, breed) {
		this.name = 'Frankie';
		this.age = '5';
		this.breed = 'Miniature Dachshund';
	}
};
const newDog = new Dog();
console.log(newDog);